===============================
Snake Oil
===============================


.. image:: https://img.shields.io/pypi/v/snake_oil.svg
        :target: https://pypi.python.org/pypi/snake_oil

.. image:: https://img.shields.io/travis/lisael/snake_oil.svg
        :target: https://travis-ci.org/lisael/snake_oil

.. image:: https://readthedocs.org/projects/snake-oil/badge/?version=latest
        :target: https://snake-oil.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/lisael/snake_oil/shield.svg
     :target: https://pyup.io/repos/github/lisael/snake_oil/
     :alt: Updates


Simple selenium runner


* Free software: GNU General Public License v3
* Documentation: https://snake-oil.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

