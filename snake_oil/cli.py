# -*- coding: utf-8 -*-

import click
from snake_oil import config
from snake_oil.suite import Suite
from .context import Context
import logging
import logging.config

DEFAULT_LOGGING = { 
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': { 
        'standard': { 
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        'console': {
            'format': '%(asctime)s [%(levelname)s] %(message)s'
        }
    },
    'handlers': { 
        'default': { 
            'level': 'DEBUG',
            'formatter': 'console',
            'class': 'logging.StreamHandler',
        },
        'output': { 
            'level': 'DEBUG',
            'formatter': 'console',
            'class': 'logging.StreamHandler',
        },
        'console': { 
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': { 
        '': { 
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True
        },
        'output': { 
            'handlers': ['output'],
            'level': 'DEBUG',
            'propagate': False
        },
    } 
}

logging.config.dictConfig(DEFAULT_LOGGING)


@click.command()
@click.option("--environment", "-e", default="main")
@click.option("--driver", "-d", default="default")
@click.argument("suite")
def main(environment, driver, suite):
    env = config.load_env(environment)
    driver = config.load_driver(driver)
    suite = config.load_suite(suite)
    env["driver"] = driver
    suite = Suite(Context(env, name="global"), suite)
    try:
        suite()
    except:
        raise
    finally:
        driver.quit()


if __name__ == "__main__":
    main(auto_envvar_prefix='SNAKE')
