import ruamel.yaml as yaml
import os
import datetime
from .context import Context


def default_env():
    return {
        "_timestamp": datetime.datetime.now().strftime("%Y%m%d%H%M%S"),
        "_reports_dir": os.path.abspath(os.sep.join([".", "reports"])),
        "_report_dir": os.sep.join(["{{_reports_dir}}", "{{_timestamp}}"]),
        "_screenshot_dir": os.sep.join(["{{_report_dir}}", "screenshots"])
    }

def _load_config(path):
    with open(path) as f:
        config = yaml.load_all(f, Loader=yaml.RoundTripLoader)
        config = next(config)
    return config

def load_env(environment):
    env = _load_config(os.sep.join([".","envs","{}.yml".format(environment)]))
    return Context(default_env(), env)

def load_driver(driver):
    conf = _load_config(os.sep.join([".","drivers","{}.yml".format(driver)]))
    b = conf["browser"]
    if b.lower().strip() == "firefox":
        from selenium.webdriver import Firefox
        d = Firefox()
        return d
    if b.lower().strip() == "chrome":
        from selenium.webdriver import Chrome
        d = Chrome()
        return d

def load_suite(suite):
    return _load_config(os.sep.join([".","suites","{}.yml".format(suite)]))
