from jinja2 import Template
from collections.abc import Mapping
import logging

output = logging.getLogger("output")


class Context(Mapping):
    def __init__(self, sub, extended=None, name=None):
        self._sub = sub
        self._dict = extended if extended else {}
        self._name = name

    def __getitem__(self, key):
        if key == self._name and self._name is not None:
            return self
        try:
            return self._dict[key]
        except KeyError:
            return self._sub[key]

    def __setitem__(self, key, value):
        self._dict[key] = value

    def __delitem__(self, key):
        try:
            del(self._dict[key])
        except KeyError:
            del(self._sub[key])

    def __iter__(self):
        if self._name is not None:
            yield self._name
        for key in {k for k in self._sub} | {k for k in self._dict}:
            yield key

    def __len__(self):
        s = {k for k in self._sub} | {k for k in self._dict}
        return len(s)

    def setdefault(self, key, value):
        try:
            return self[key]
        except KeyError:
            self[key] = value
            return value

    @property
    def driver(self):
        return self['driver']

    def render(self, tmpl):
        output.debug("Rendering `%s`" % tmpl)
        result = Template(tmpl).render(self)
        while result != tmpl:
            tmpl = result
            result = Template(tmpl).render(self)
        output.debug(result)
        return result
