from snake_oil.directive.base import Step, Assert
from snake_oil.context import Context
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import os
import logging
import time

output = logging.getLogger("output")


def _get_finder(css=None, xpath=None, id=None, default=None):
    # TODO: add all find method from
    # https://selenium-python.readthedocs.io/locating-elements.html#locating-elements
    if css:
        return (By.CSS_SELECTOR, css)
    elif xpath:
        return (By.XPATH, xpath)
    elif id:
        return (By.ID, id)
    elif default:
        return default
    raise ValueError("No suitable finder")
    
def chunk_text(text):
    keys = [k for k in dir(Keys) if k.isupper()]
    real_keys = [getattr(Keys, k) for k in keys]
    chunks = [text]
    for k in keys:
        new = []
        for c in chunks:
            if c in real_keys:
                new.append(c)
                continue
            subs = c.split("/%s/" % k)
            if len(subs) == 1:
                new.append(c)
                continue
            if subs[-1] == '':
                subs.pop()
            for sub in subs:
                if sub != '':
                    new.append(sub)
                new.append(getattr(Keys, k))
        chunks = new
    return chunks


class _ValueStep(Step):
    def __init__(self, register):
        self._key = register

    def _register(self, env, value):
        if self._key is not None:
            path = self._key.strip().split(".")
            ctx = env
            while len(path) > 1:
                ctx = ctx[path.pop(0)]
            ctx[path.pop(0)] = value


class Back(Step):
    def __init__(self, num=1):
        self._num = num

    def __call__(self, env):
        for num in range(self._num):
            env.driver.back()


class Forward(Step):
    def __init__(self, num=1):
        self._num = num

    def __call__(self, env):
        for num in range(self._num):
            env.driver.forward()


class Pause(Step):
    def __init__(self, delay):
        self.delay = float(delay)

    def __call__(self, env):
        time.sleep(self.delay)

class Debug(Step):
    def __init__(self, msg):
        self.msg = msg

    def __call__(self, env):
        output.debug(self.msg)


class _ContentPredicateMixin:
    def __init__(self, exact=None, contains=None, not_contains=None, iexact=None, icontains=None, isnot=None, **kwargs):
        self._method = None
        try:
            self._method, self._predicate, self._msg, self._bad_msg = [p for p in [
               (self.exact, exact, "`{value}` == `{predicate}`", "`{value}` != `{predicate}`"),
               (self.isnot, isnot, "`{value}` != `{predicate}`", "`{value}` == `{predicate}`"),
               (self.contains, contains, "`{value}` contains `{predicate}`", "`{value}` does not contain `{predicate}`"),
               (self.not_contains, not_contains, "`{value}` does not contain `{predicate}`", "`{value}` contains `{predicate}`"),
               (self.iexact, iexact, "`{value}` == `{predicate}`", "`{value}` != `{predicate}`"),
               (self.icontains, icontains, "`{value}` icontains `{predicate}`", "`{value}` does not icontain `{predicate}`")] if p[1]][0]
        except IndexError:
            self._method is None

    def exact(self, value):
        return self._predicate == value

    def isnot(self, value):
        return self._predicate != value

    def contains(self, value):
        return self._predicate in value

    def not_contains(self, value):
        return self._predicate not in value

    def iexact(self, value):
        return self._predicate.lower() == value.lower()

    def icontains(self, value):
        return self._predicate.lower() in value.lower()

    def _assert_value(self, val):
        if self._method is None:
            return
        if not self._method(val):
            output.error(self._bad_msg.format(value=val, predicate=self._predicate))
            raise AssertionError
        output.info(self._msg.format(value=val, predicate=self._predicate))


class Text(_ValueStep, _ContentPredicateMixin):
    def __init__(self, **kwargs):
        _ValueStep.__init__(self, kwargs.pop("register", None))
        _ContentPredicateMixin.__init__(self, **kwargs)
        output.debug(kwargs)

    def __call__(self, env):
        txt = env['element'].elem.text
        self._register(env, txt)
        self._assert_value(txt)


class Title(_ContentPredicateMixin, Step):
    def __call__(self, env):
        self._assert_value(env.driver.title)


class Source(_ContentPredicateMixin, Step):
    def __call__(self, env):
        self._assert_value(env.driver.page_source)


class Page(Step):
    def __init__(self, url=None):
        self.url = url

    def __call__(self, env):
        if self.url is not None:
            env.driver.get(self.url)
        env['element'] = env.driver
        env["page"] = self


class Wait_until(Step):
    def __init__(self, predicates):
        import ipdb; ipdb.set_trace()

    def __call__(env):
        pass


class Screenshot(Step):
    def __init__(self, name):
        self.name = name

    def __call__(self, env):
        dest_dir = env.render('{{ _screenshot_dir }}')
        os.makedirs(dest_dir, exist_ok=True)
        output.debug("Created snapshot dir %s" % dest_dir)
        dest_file = os.path.abspath(os.sep.join([dest_dir, self.name + ".png"]))
        output.debug(dest_file)
        env.driver.save_screenshot(dest_file)


class Fill_by_id(Step):
    def __init__(self, **kwargs):
        self.fields = kwargs

    def __call__(self, env):
        elem = env["element"]
        for id, value in self.fields.items():
            field = elem._find_one(id=id)
            for chunk in chunk_text(value):
                field.send_keys(chunk)


class Element(Step):
    def __init__(self, css=None, xpath=None, id=None, wait=None, elem=None):
        self.finder = _get_finder(css, xpath, id)
        self.wait = wait
        self.elem = elem

    def __call__(self, env):
        if self.elem is None:
            env['element'] = self
            if self.wait is not None:
                env.driver.implicitly_wait(float(self.wait))
            self.elem = env.driver.find_element(*self.finder)
            env.driver.implicitly_wait(0.1)

    def _find_one(self, css="", xpath="", id="", **kwargs):
        return self.elem.find_element(
            *_get_finder(css, xpath, id, (By.CSS_SELECTOR, "*")))

    def _find_all(self, css="", xpath="", id="", **kwargs):
        return self.elem.find_elements(
            *_get_finder(css, xpath, id, (By.CSS_SELECTOR, "*")))


class Send_keys(Step):
    def __init__(self, text):
        self._chunks = chunk_text(text)

    def __call__(self, env):
        for chunk in self._chunks:
            env["element"].elem.send_keys(chunk)


class Submit(Step):
    def __call__(self, env):
        env["element"].elem.submit()


class Clear(Step):
    def __call__(self, env):
        env["element"].elem.clear()


class Click(Step):
    def __init__(self, css=None):
        self.target = css

    def __call__(self, env):
        element = env['element']
        if self.target:
            elem = element._find_one(css=self.target)
        else:
            elem = element.elem
        elem.click()


class Form(Element):
    def clear(self, env):
        for c in self._find_all(xpath=".//input[@name]") :
            c.clear()
        for c in self._find_all(xpath=".//textarea[@name]") :
            c.clear()


class Test(Step):
    def __init__(self, title):
        self.title = title

    def _prepare_env(self, env):
        return Context(env, dict({}), "test")

    def __call__(self, env):
        current_ord = env.setdefault("test_ord", 0)
        env['test_ord'] = current_ord + 1
        output.info("Test %d: %s" % (env['test_ord'], self.title))


class Doc(Step):
    def __init__(self, title):
        self.title = None

    def __call__(self, env):
        pass


class End_doc(Step):
    def __init__(self):
        pass

    def __call__(self, env):
        pass


class Fill(Step):
    def __init__(self, id, text, submit=False):
        self.id = id
        self.text = text
        self.submit = submit

    def __call__(self, env):
        element = env.driver.find_element_by_id(self.id)
        element.send_keys(self.text)
        if self.submit:
            element.submit()
