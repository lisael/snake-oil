import logging
from .registry import StepBase, AssertBase

output = logging.getLogger("output")

class Step(metaclass=StepBase):
    def _prepare_env(self, env):
        return env

    _clean_env = _prepare_env

    def _is_assert(self):
        return False


class Assert(Step):
    def _is_assert(self):
        return True

def is_assert(fn):
    fn._is_assert = True
    return fn
