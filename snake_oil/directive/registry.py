class StepBase(type):
    steps = {}
    def __new__(cls, name, bases, attrs):
        new = type.__new__(cls, name, bases, attrs)
        if name == "Step":
            return new
        cls.steps[name.lower()] = new
        return new

steps = StepBase.steps

class AssertBase(type):
    directives = {}
    def __new__(cls, name, bases, attrs):
        new = type.__new__(cls, name, bases, attrs)
        if name == "Assert":
            return new
        cls.directives[name.lower()] = new
        return new

asserts = AssertBase.directives
