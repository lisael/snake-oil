import logging

from .context import Context
from .directive.registry import steps
from .directive.base import Step


output = logging.getLogger("output")


def render_args(args, kwargs, env):
    # render templates
    rargs = []
    for arg in args:
        rargs.append(env.render(arg) if isinstance(arg, str) else arg)
    rkwargs = {k: env.render(v) for k, v in kwargs.items() if isinstance(v, str)}
    for k, v in kwargs.items():
        if k not in rkwargs:
            rkwargs[k] = v
    return (
        rargs,
        rkwargs
    )


def call_step(env, step, *args, **kwargs):
    items = kwargs.pop("with_items", None)
    if items is None:
        args, kwargs = render_args(args, kwargs, env)
        step_inst = step(*args, **kwargs)
        is_assert = False
        if isinstance(step_inst, (Step, FuncNode)):
            env = step_inst._prepare_env(env)
            is_assert = step_inst._is_assert()
        if is_assert:
            if env.get('test', None) is not None:
                count = env['test'].setdefault('_assert_count', 0)
                env['test']['_assert_count'] = count + 1
        step_inst(env)
        if is_assert:
            if env.get('test', None) is not None:
                count = env['test'].setdefault('_assert_done', 0)
                env['test']['_assert_done'] = count + 1
        return env
    else:
        for item in items:
            env = Context(env, dict(item=item))
            env = call_step(step, *args, **kwarg)
            del env['item']
        return env


class FuncNode:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        return self.do_call

    def do_call(self, env):
        self.func(env, *self.args, **self.kwargs)

    def _prepare_env(self, env):
        return env

    _clean_env = _prepare_env

    def _is_assert(self):
        if hasattr(self.func, "_is_assert"):
            return getattr(self.func, "_is_assert")
        return False


def call_list(env, lst, filename="<unknown>"):
    lineno = 0
    for dir_def in lst:
        old_env = env
        try:
            lineno = dir_def._yaml_line_col.line
        except:
            pass
        then = None
        if isinstance(dir_def, str):
            name = dir_def
            args = []
            kwargs = {}
        else:
            it = iter(dir_def.items())
            name, arg = next(it)
            args = [arg] if arg else []
            kwargs = {k: v for k, v in it}
            then = kwargs.pop('then', None)
        
        output.debug("Calling %s" % name)
        elem = env.get("element", object())
        if hasattr(elem, name) and callable(getattr(elem, name)):
            env =call_step(env, FuncNode(getattr(elem, name)), *args, **kwargs)
        else:
            try:
                step = steps[name]
            except KeyError:
                try:
                    step = asserts[name]
                except KeyError:
                    output.error("Step `{name}` not found at line {lineno} in {filename}".format(name=name, lineno=lineno, filename=filename))
                    raise
            env = call_step(env, step, *args, **kwargs)
        if then is not None:
            call_list(Context(env), then, filename)
        env = old_env
