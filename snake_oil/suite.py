from .contrib.directives import *  # noqa
from .node import call_list
from .context import Context

class Suite:
    def __init__(self, env, suite):
        self.session = env
        self.env = Context(self.session, dict(session=self.session))
        self._children = suite

    def __call__(self):
        # import ipdb; ipdb.set_trace()
        call_list(self.env, self._children)
